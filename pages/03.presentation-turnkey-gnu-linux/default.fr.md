---
title: 'Présentation TurnKey GNU/Linux'
media_order: Turnkey_logo.jpg
---

![source:https://www.turnkeylinux.org/](Turnkey_logo.jpg)

## Qu'est-ce que TurnKey GNU/Linux?
TurnKey GNI/Linux est une bibliothèque d'image Linux qui contient différents types d'applications concernant le **développement web**, les **bases de données** et bien d'autres choses encore.
![](img1.jpg)
## L'utilisation
Cet outil ne vise pas une clientèle  particulière, au contraire leur but est d'aider tout le monde en offrant des application gratuites, faciles à utiliser et sécuritaires. 

Les applications proposées peuvent être téléchargées sur un **Bare Metal**, une **machine virtuelle** et sur le **Cloud** quand vous en avez besoin. L'utilisation de TurnKey GNU/Linux est conseillé, car il vous facilite la tâche en vous proposant des images contenant déjà tous les configurations nécessaires pour rouler l'application et vous pouvez le télécharger plusieurs fois gratuitement. En cas le téléchargement, vous pouvez cliquer sur ce lien pour suivre les étapes à suivre: _[installation](../installation)_.

source:[https://www.turnkeylinux.org/](https://www.turnkeylinux.org/)