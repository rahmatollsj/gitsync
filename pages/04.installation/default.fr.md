---
title: Installation
media_order: 'img2.jpg,screenshot.png,screenshot2.png,screenshot3.png'
---

## Pour télécharger une application avec TurnKey GNU/Linux
![image d'une personne qui réfléchit](img2.jpg)
1.  Tout d'abord, si vous ne savez pas ce qu'est TurnKey GNU/Linux je vous invite à lire la documentation en cliquant sur ce lien: [documentation](../presentation-turnkey-gnu-linux).
2.  Aller sur le site de TrunKey GNU/Linux: [https://www.turnkeylinux.org/](https://www.turnkeylinux.org/)
3.  Dans l'onglet application, choisissez l'application que vous voulez installer.
![capture d'écran du site](screenshot.png)
5.  Un fois que vous avez choisi votre application, vous pouvez l'installer sur un Bare Metal, une machine virtuelle et sur un Cloud.
![capture d'écran du site](screenshot2.png)
6. Par exemple, on voudrait installer une application sur Docker. Vous devez cliquer sur lien Docker.
7. Une fois sur le site de Docker, vous inscrivez le code affiché dans la console de Docker.
![capture d'écran du site](screenshot3.png)

Voici une vidéo qui vous permettera de voir plus en détail les étapes à suivre (installation d'un serveur de fichier sur VirtualBox):
[plugin:youtube](https://www.youtube.com/watch?v=UUdahT-FELM)

source:[https://www.turnkeylinux.org/](https://www.turnkeylinux.org/), [https://hub.docker.com/r/turnkeylinux/lamp](https://hub.docker.com/r/turnkeylinux/lamp), [https://www.youtube.com/watch?v=UUdahT-FELM](https://www.youtube.com/watch?v=UUdahT-FELM)